﻿using System.Linq;

namespace Domain
{
    public class StringCalculator
    {
        private readonly int defaultValue = 0;

        public int Add(string input)
        {
            if (string.IsNullOrEmpty(input) || HasWrongDelimiter(input))
            {
                return defaultValue;
            }

            if (HasCustomDelimiter(input))
            {
                var index = input.IndexOf("\n");
                var delimiter = GetDelimiter(input);

                input = input.Substring(index);
                var numbers = input.Split(delimiter);


                return Count(numbers);
            }

            if (HasDefaultDelimiter(input))
            {
                var numbers = input.Split(',', '\n');

                return Count(numbers);
            }


            return ParseSingleNumber(input);
        }

        private int Count(string[] numbers)
        {
            var message = string.Empty;

            foreach (var number in numbers)
            {
                if (ParseSingleNumber(number) < 0)
                {
                    message = string.Concat(message, " ", number);
                }
            }

            if (string.IsNullOrEmpty(message))
            {
                return numbers.Sum(ParseSingleNumber);
            }

            throw new NegativeNumberException("Input has negative numbers" + message);
        }

        private bool HasCustomDelimiter(string input)
        {
            return input.StartsWith("//");
        }

        private bool HasWrongDelimiter(string input)
        {
            return input.Contains("\n,");
        }

        private bool HasDefaultDelimiter(string input)
        {
            return input.Contains(",") || input.Contains('\n');
        }

        private int ParseSingleNumber(string input)
        {
            return int.Parse(input);
        }

        private char[] GetDelimiter(string input)
        {
            var index = input.IndexOf("\n");
            var tmp = input.Remove(index);
            var splited = tmp.Split('[', ']');
            var delimiter = splited[1];

            return delimiter.ToCharArray();
        }
    }
}