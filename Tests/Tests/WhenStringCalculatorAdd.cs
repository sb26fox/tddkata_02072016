﻿using Domain;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class WhenStringCalculatorAdd
    {
        [SetUp]
        public void SetUp()
        {
            calculator = new StringCalculator();
        }

        private StringCalculator calculator;

        [Test]
        public void Return1IfInputIs1()
        {
            var sum = calculator.Add("1");

            Assert.AreEqual(1, sum);
        }

        [Test]
        public void ReturnAnyNumberIfInputIsAnyNumber()
        {
            var sum = calculator.Add("2");

            Assert.AreEqual(2, sum);
        }

        [Test]
        public void ReturnSumOfSeveralNumberIfInputHasCustomDelimiter()
        {
            var sum = calculator.Add("//[;]\n1;2;3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfSeveralNumbersIfInputHasNewLineDelimiter()
        {
            var sum = calculator.Add("1\n2,3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfSeveralNumbersIfInputHasSeveralNumbers()
        {
            var sum = calculator.Add("1,2,3");

            Assert.AreEqual(1 + 2 + 3, sum);
        }

        [Test]
        public void ReturnSumOfTwoNumbersIfInputHasTwoNumbers()
        {
            var sum = calculator.Add("1,2");

            Assert.AreEqual(1 + 2, sum);
        }

        [Test]
        public void ReturnZeroIfInputHasWrongDelimiter()
        {
            var sum = calculator.Add("1\n,2,3");

            Assert.AreEqual(0, sum);
        }

        [Test]
        public void ReturnZeroIfInputIsEmpty()
        {
            var sum = calculator.Add(string.Empty);

            Assert.AreEqual(0, sum);
        }

        [Test]
        public void ThrowNegativeNumberExceptionIfInputHasNegativeNumbers()
        {

            Assert.Throws<NegativeNumberException>(()=>calculator.Add("-1,2,3"));

        }
    }
}